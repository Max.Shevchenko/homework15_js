let handSec = document.getElementById('handSec');
let handMin = document.getElementById('handMin');
let handHour = document.getElementById('handHour');
let handMili = document.getElementById('handMili');
let mili = document.getElementById('mi');
let sec = document.getElementById('s');
let min = document.getElementById('m');
let h = document.getElementById('h');
let pause = false;
let pauseCount = 0;
let timer1;
let timer2;
let df = 0;
let dfCount = 0;
let timeDfCount;



function IntervalTimer(callback, interval) {
    let timerId, startTime, remaining = 0;
    let state = 0;

    this.pause = function () {
        if (state !== 1) return;

        remaining = interval - (new Date() - startTime);
        window.clearInterval(timerId);
        state = 2;
    };

    this.resume = function () {
        if (state !== 2) return;

        state = 3;
        window.setTimeout(this.timeoutCallback, remaining);
    };

    this.timeoutCallback = function () {
        if (state !== 3) return;

        callback();

        startTime = new Date();
        timerId = window.setInterval(callback, interval);
        state = 1;
    };

    startTime = new Date();
    timerId = window.setInterval(callback, interval);
    state = 1;
}





function initClock() {
    let start = Date.now();
    timer1 = new IntervalTimer(function () {
            let delta = Date.now() - start - df;
            let secDeg = (delta / 1000) * 6;
            let minDeg = ((delta / 1000) * 6) / 60;
            let hourDeg = (((delta / 1000) * 6) / 60) / 60;
            let miliDeg = delta * 0.36;
            handSec.style.transform = `rotate(${secDeg}deg)`;
            handMin.style.transform = `rotate(${minDeg}deg)`;
            handHour.style.transform = `rotate(${hourDeg}deg)`;
            handMili.style.transform = `rotate(${miliDeg}deg)`;

    }, 1)
}



function timerDig() {
    let start = Date.now();
    timer2 = new IntervalTimer(function () {
            let delta = Date.now() - start;
            if (delta < 1000) {
                if (delta < 10) {
                    mili.innerHTML = '00'.concat(delta.toString());
                } else if (delta < 100) {
                    mili.innerText = '0'.concat(delta.toString());
                } else {
                    mili.innerText = delta.toString();
                }
            } else {
                mili.innerText = '000';
                start = Date.now();
                let sumSec = parseInt(sec.innerText) + 1;
                if (sumSec < 10) {
                    sec.innerText = `0${sumSec}`;
                } else {
                    sec.innerText = `${sumSec}`;
                }
                if (parseInt(sec.innerText) > 59) {
                    sec.innerText = '00';
                    let sumMin = parseInt(min.innerText) + 1;
                    if (sumMin < 10) {
                        min.innerText = `0${sumMin}`;
                    } else {
                        min.innerText = `${sumMin}`;
                    }
                    if (parseInt(min.innerText) > 59) {
                        min.innerText = '00';
                        let sumH = parseInt(h.innerText) + 1;
                        if (sumH < 10) {
                            h.innerText = `0${sumH}`;
                        } else {
                            h.innerText = `${sumH}`;
                        }
                    }
                }
            }
    }, 1)
}

function startBut() {
    let but = document.getElementById('startBut');
    if (but.innerText === 'Start') {
        if (pauseCount === 0) {
            pause = false;
            timerDig();
            initClock();
            but.innerText = 'Pause';
        } else {
            clearInterval(timeDfCount);
            timer1.resume();
            timer2.resume();
            but.innerText = 'Pause';
        }
    } else {
        pauseCount++;
        timer1.pause();
        let start = Date.now();
        if (df === 0) {
            timeDfCount = setInterval(function () {
                df = Date.now() - start;
                dfCount++;
                sessionStorage.setItem('df', df.toString());
                sessionStorage.setItem('start', start.toString());
            }, 1);
        } else {
            timeDfCount = setInterval (function () {
                let newDf = Date.now() - start;
                df = parseInt(sessionStorage.getItem('df')) + newDf;
            }, 1);
            sessionStorage.setItem('df', df.toString());
        }
        timer2.pause();
        but.innerText = 'Start';
    }
}


function clearAll() {
    window.location.reload(true);
}





